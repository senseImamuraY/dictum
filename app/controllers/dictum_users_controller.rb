class DictumUsersController < ApplicationController

    before_action :logged_in_user, only: [:edit, :update, :destroy]
    before_action :correct_user,   only: [:edit, :update, :destroy]
    before_action :admin_user,     only: :destroy
  
    # def show
    #   @dictum_user = DictumUser.find(params[:id])
    #   @gogakus = @dictum_user.gogakus.paginate(page: params[:page])
    # end
  
    def new
      @dictum_user = DictumUser.new
    end
  
    def create
      @dictum_user = DictumUser.new(dictum_user_params)
      if @dictum_user.save
        log_in @dictum_user
        flash[:success] = "DICTUMへようこそ!"
        redirect_to root_url
      else
        flash[:danger] = @dictum_user.errors.full_messages if @dictum_user.errors.any?
        redirect_to new_dictum_user_path
      end
    end
  
    def edit
      @dictum_user = DictumUser.find(params[:id])
    end
  
    def update
      @dictum_user = DictumUser.find(params[:id])
      if @dictum_user.update(dictum_user_params)
        flash[:success] = "更新完了"
        redirect_to root_url
      else
        flash[:danger] = @dictum_user.errors.full_messages if @dictum_user.errors.any?
        redirect_to edit_dictum_user_path
      end
    end
  
    def destroy
      DictumUser.find(params[:id]).destroy
      flash[:success] = "ユーザー削除完了"
      redirect_to root_url
    end
  
      private
  
        def dictum_user_params
          params.require(:dictum_user).permit(:name, :email, :password,
                                      :password_confirmation)
        end
  
        # beforeアクション
  
      # ログイン済みユーザーかどうか確認
        # def logged_in_user
        #   unless logged_in?
        #     store_location
        #     flash[:danger] = "Please log in."
        #     redirect_to login_url
        #   end
        # end
  
        # 正しいユーザーかどうか確認
        def correct_user
          @dictum_user = DictumUser.find(params[:id])
          redirect_to(root_url) unless current_user?(@dictum_user)
        end
  
        # 管理者かどうか確認
        def admin_user
          redirect_to(root_url) unless current_user.admin?
        end
  end
  