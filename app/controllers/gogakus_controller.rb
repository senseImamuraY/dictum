class GogakusController < ApplicationController
    # before_action :set_gogaku, only:[:show,:edit,:update,:destroy]
    before_action :logged_in_user
    # , only: [:index,:show,:new,:create, :destroy]
    before_action :correct_user_g,   only: [:show, :edit, :update, :destroy]
    # def index
    #     @gogakus = Gogaku.all
    # end

    def show
        
        # @gogaku = Gogaku.find(params[:id])
       render :layout => 'g_show'
        # byebug
        
    end 
    
# debugger
        # gon.file = @gogaku.file
    def new
        @gogaku = Gogaku.new
    end

    def create
        @gogaku = current_user.gogakus.build(gogaku_params)
        # @gogaku.file = params[:gogaku][:file]
        # byebug
        if @gogaku.save
            flash[:success] = "登録完了!"
            redirect_to root_url
        else
            @feed_items = current_user.feed.paginate(page: params[:page])
            flash[:danger] = @gogaku.errors.full_messages if @gogaku.errors.any?
            redirect_to new_gogaku_path
        end
        # gogaku_params = params.require(:gogaku).permit(:subject,:body,:num)
        # @gogaku = Gogaku.new(gogaku_params)
        # if @gogaku.save
        #     flash[:notice] = "教科書登録成功"
        #     redirect_to gogakus_path
        # else
        #     flash.now[:alert] = "登録失敗"
        #     render :new
        # end
    end

    def edit
        # @gogaku = Gogaku.find(params[:id])
    end

    def update
        # @gogaku = Gogaku.find(params[:id])

        gogaku_params = params.require(:gogaku).permit(:subject, :body, :file, :answer)
        if @gogaku.update(gogaku_params)
            flash[:notice]="更新成功"
            # @feed_items = current_user.feed.paginate(page: params[:page])
            redirect_to root_url
        else
            flash[:danger] = @gogaku.errors.full_messages if @gogaku.errors.any?
            
            redirect_to controller: :gogakus,action: :edit, id: @gogaku.id
        end
    end

    def destroy
        # @gogaku = Gogaku.find(params[:id])
        @gogaku.destroy
        flash[:notice]="削除しました"
        redirect_to request.referrer || root_url
    end

    private

        # def set_gogaku
        #     @gogaku ||= Gogaku.find(params[:id])
        # end



        def gogaku_params
            params.require(:gogaku).permit(:subject, :body, :file, :answer,)
        end

        def correct_user_g
            @gogaku = current_user.gogakus.find_by(id: params[:id])
            redirect_to root_url if @gogaku.nil?
        end
end
