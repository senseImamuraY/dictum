class SessionsController < ApplicationController

    def new
    end
  
    def create
      dictum_user = DictumUser.find_by(email: params[:session][:email].downcase)
      if dictum_user && dictum_user.authenticate(params[:session][:password])
        log_in dictum_user
        redirect_back_or root_url
      else
        flash[:danger] = "Invalid email/password combination"
        redirect_to login_path
      end
    end
  
    def destroy
      log_out
      redirect_to root_url
    end
  end
  