class StaticPagesController < ApplicationController

  before_action :logged_in_user, only: [:quick]

    def home
      if logged_in?
        @gogaku = current_user.gogakus.build
        @feed_items = current_user.feed.paginate(page: params[:page])
      end
    end
  
    def help
      
    end
  
    def about
     
    end
  
    # def contact
      
    # end

    def quick
      render :layout => 'g_show'
    end

    def demo
      render :layout => 'g_show'
    end
  end
  