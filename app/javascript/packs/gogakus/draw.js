var wavesurfer = WaveSurfer.create({
            barRadius: 3,
            cursorWidth: 1,
            height: 230,barMinHeight: 1,
            barGap: 1,
            container: '#waveform',
            waveColor: 'gray',
            progressColor: 'orange',
            scrollParent: true,
            backend: 'MediaElementWebAudio',
            plugins: [
                WaveSurfer.regions.create({
               
                    regions: [
                        {
                            start: 5,
                            end: 7,
                            loop: false,
                            color: 'hsla(200, 50%, 70%, 0.4)',
                        }
                    ],
           
                })
            ]
    });   
    

 wavesurfer.load('<%= rails_blob_path(@gogaku.file) %>');    

// audio comes from different domain so need to do this (not related to the bug)
        const corsAudio = new Audio('<%= rails_blob_path(@gogaku.file) %>');
        corsAudio.crossOrigin = true;
    
        wavesurfer.load(corsAudio);
    
        wavesurfer.on('ready', () => {
        console.log('ready');
        });
    
        
        // function playbackRate() {
        // rate = rate === 1 ? 2 : 1;
        // wavesurfer.setPlaybackRate(rate);
        // document.getElementById("rate").innerText = "速度: " + rate;
        // }
    
        function playPause() {
        wavesurfer.playPause();
    }
    
   
    
    
        
    
        
           
        
    

    
        
    
    
