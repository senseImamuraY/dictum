var wavesurfer = WaveSurfer.create({
    container: '#waveform',
    waveColor: 'hotpink',
    progressColor: 'orange',
    scrollParent: true
});

// const forms = document.forms.GogakuForm;
// const Gogaku_file = forms.title.value;

// let a = gon.file;

let sample = function () {
    let t = document.getElementById('audio_file').file;
    let files = t.file;
    let file = URL.createObjectURL(files);
    document.writeln(file);
    var slider = document.querySelector('#slider');

    slider.oninput = function () {
        var zoomLevel = Number(slider.value);
        wavesurfer.zoom(zoomLevel);
    };
    wavesurfer.load(file);
};

sample();
wavesurfer.on('ready', function () {
    wavesurfer.play();
});

$("#btnStartPause").click(
    play = function () {
        wavesurfer.playPause();
    }
);