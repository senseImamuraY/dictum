class Gogaku < ApplicationRecord
    belongs_to :dictum_user
    default_scope -> { order(created_at: :desc) }
    validates :subject,presence:true
    validates :body,   presence:true
    has_one_attached :file
    validates :dictum_user_id, presence: true
    validates :file,presence:true
    validates :file,size:         { less_than: 5.megabytes,
                                      message: "should be less than 5MB" }
end
