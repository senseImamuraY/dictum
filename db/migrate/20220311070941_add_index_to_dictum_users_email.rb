class AddIndexToDictumUsersEmail < ActiveRecord::Migration[6.1]
  def change
    add_index :dictum_users, :email, unique: true
  end
end
