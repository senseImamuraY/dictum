class AddPasswordDigestToDictumUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :dictum_users, :password_digest, :string
  end
end
