class AddAdminToDictumUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :dictum_users, :admin, :boolean,default: false
  end
end
