class CreateGogakus < ActiveRecord::Migration[6.1]
  def change
    create_table :gogakus do |t|
      t.string :subject
      t.text :body
      t.string :file
      t.text :answer

      t.references :dictum_user, type: :bigint, foreign_key: true
      t.timestamps
    end
    add_index :gogakus, [:dictum_user_id, :created_at]
  end
end
