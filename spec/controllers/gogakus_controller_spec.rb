require 'rails_helper'

RSpec.describe GogakusController, type: :controller do
    describe "#show" do
        context "as an authorized user" do
            before do
                @dictum_user = FactoryBot.create(:dictum_user)
                @gogaku = FactoryBot.create(:gogaku, dictum_user: @dictum_user)
            end
            it "responds successfully" do
                log_in @dictum_user
                get :show, params: {id: @gogaku.id}
                # correct_user_g
                
                expect(response).to be_successful
                # expect(DictumUser).to receive(:find).with(@gogaku.id).and_return (@gogaku)
            end
        end
        context "as an unauthorized user" do
            before do
                @dictum_user = FactoryBot.create(:dictum_user)
                other_user = FactoryBot.create(:dictum_user)
                @gogaku = FactoryBot.create(:gogaku, dictum_user: other_user)
            end
            it "redirect to the root_url" do
                log_in @dictum_user
                get :show, params: { id: @gogaku.id }
                expect(response).to redirect_to root_url
            end
        end
    end 
    describe "#create" do
        context "as an authenticated user" do
            before do
                @dictum_user = FactoryBot.create(:dictum_user)
            end
            context "with valid attributes" do
                it "adds a project" do
                    expect(@dictum_user.gogakus.count).to eq 0
                    gogaku_params = FactoryBot.attributes_for(:gogaku,:file)
                    log_in @dictum_user
                    # current_user
                    # expect(@dictum_user.gogakus.count).to eq 0
                    expect {
                        post :create, params: { gogaku: gogaku_params ,dictum_user: @dictum_user}
                }.to change(@dictum_user.gogakus,:count).by(1)
                    # expect(response).to redirect_to new_gogaku_path
                    # expect(@dictum_user.gogakus.count).to eq 1
                end
            end
            context "with invalid attribtes" do
                it "does not add a gogaku" do
                    gogaku_params = FactoryBot.attributes_for(:gogaku, body: nil)
                    log_in @dictum_user
                    expect {
                        post :create, params: { gogaku: gogaku_params }
                }.to_not change(@dictum_user.gogakus, :count)
                end
            end
            
        end
        context "as a guest" do
            it "returns a 302 response" do
                gogaku_params = FactoryBot.attributes_for(:gogaku)
                post :create, params: { gogaku: gogaku_params}
                expect(response).to have_http_status "302"
            end
        end
        context "as a guest" do
            it "redirects to login page" do
                gogaku_params = FactoryBot.attributes_for(:gogaku)
                post :create, params: { gogaku: gogaku_params}
                expect(response).to redirect_to login_path
            end
        end
    end
    describe "#update" do
        context "as an authorized user" do
            before do
                @dictum_user = FactoryBot.create(:dictum_user)
                @gogaku = FactoryBot.create(:gogaku, dictum_user: @dictum_user)
            end
            it "updates a project" do
                t_gogaku_params = FactoryBot.attributes_for(:gogaku, subject: "Math")
                log_in @dictum_user
                patch :update, params: { id: @gogaku.id, gogaku: t_gogaku_params }
                expect(@gogaku.reload.subject).to eq "Math"
            end
        end
        context "as an unauthorized user" do
            before do
                @dictum_user = FactoryBot.create(:dictum_user)
                other_user = FactoryBot.create(:dictum_user)
                @gogaku = FactoryBot.create(:gogaku, dictum_user: other_user, subject:"Biology")
            end
            it "does not update the gogaku" do
                t_gogaku_params = FactoryBot.attributes_for(:gogaku, subject: "Botany")
                log_in @dictum_user
                patch :update, params: { id:@gogaku.id, gogaku:t_gogaku_params}
                expect(@gogaku.reload.subject).to eq "Biology"
            end
            it "redirects to the edit" do
                t_gogaku_params = FactoryBot.attributes_for(:gogaku)
                log_in @dictum_user
                patch :update, params: { id:@gogaku.id, gogaku:t_gogaku_params}
                expect(response).to redirect_to root_url
            end
        end
        context "as a guest" do
            before do
                @gogaku = FactoryBot.create(:gogaku)
            end
            it "returns a 302 response" do
                gogaku_params = FactoryBot.attributes_for(:gogaku)
                patch :update, params: { id: @gogaku.id,gogaku: gogaku_params }
                expect(response).to have_http_status "302"
            end
            it "redirect to the login" do
                gogaku_params = FactoryBot.attributes_for(:gogaku)
                patch :update, params: { id: @gogaku.id,gogaku: gogaku_params }
                expect(response).to redirect_to login_path
            end
        end
    end
    describe "#destroy" do
        context "as an authorized user" do
            before do
                @dictum_user = FactoryBot.create(:dictum_user)
                @gogaku = FactoryBot.create(:gogaku, dictum_user: @dictum_user)
            end
            it "deletes a project" do
                log_in @dictum_user
                expect {
                    delete :destroy, params: { id:@gogaku.id }
            }.to change(@dictum_user.gogakus,:count).by(-1)
            end
        end
        context "if user is deleted" do
            
            before do
                @dictum_user = FactoryBot.create(:dictum_user)
                @other_user = FactoryBot.create(:dictum_user,:admin)
                # gogaku_params = FactoryBot.attributes_for(:gogaku,dictum_user: @dictum_user)
                # @dictum_user.gogakus.create(gogaku_params)
                FactoryBot.create(:gogaku, dictum_user: @dictum_user)
                
            end
                context "as an admin user or nomal user" do
                    it "be deleted for being admin" do
                    expect(@dictum_user.gogakus.count).to eq 1
                    log_in @other_user
                    admin_user
                    expect{
                        @dictum_user.destroy
                }.to change(Gogaku,:count).by(-1)
                end
                    it "be not deleted for being not admin" do
                    log_in @dictum_user
                    delete :destroy, params: {id:@other_user.id}
                    expect(response).to redirect_to root_url
                end
            end
            
        end
        context "as an unauthorized user" do
            before do
                @dictum_user = FactoryBot.create(:dictum_user)
                other_user = FactoryBot.create(:dictum_user)
                @gogaku = FactoryBot.create(:gogaku, dictum_user: other_user)
            end
            it "does not delete the gogaku" do
                log_in @dictum_user
                expect {delete :destroy, params: {id: @gogaku.id}
            }.to_not change(Gogaku, :count)           
            end
            it "redirects to the root_url" do
                log_in @dictum_user
                delete :destroy, params: {id:@gogaku.id}
                expect(response).to redirect_to root_url
            end
        end
        context "as a guest" do
            before do
                @gogaku = FactoryBot.create(:gogaku)
            end
            it "returns a 302 response" do
                delete :destroy, params: {id:@gogaku.id}
                expect(response).to have_http_status "302"
            end
            it "redirects to the login" do
                delete :destroy, params: {id:@gogaku.id}
                expect(response).to redirect_to login_path
            end
            it "does not delete the gogaku" do
                expect {delete :destroy, params: {id:@gogaku.id}
            }.to_not change(Gogaku, :count)
            end
        end
    end

end
