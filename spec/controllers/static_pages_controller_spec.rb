require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
    describe "#home" do
        it "resonds successfully" do
            get :home
            expect(response).to be_successful
        end
    end
    describe "#help" do
        it "resonds successfully" do
            get :help
            expect(response).to be_successful
        end
    end
    describe "#about" do
        it "resonds successfully" do
            get :about
            expect(response).to be_successful
        end
    end
    describe "#quick" do
        context "as an authenticated usre" do
            before do
                @dictum_user = FactoryBot.create(:dictum_user)
            end
            it "resonds successfully" do
                log_in @dictum_user
                get :quick
                expect(response).to be_successful
            end
        end

        context "as a guest" do
            it "returns a 302 response" do
                get :quick
                expect(response).to have_http_status "302"
            end
            it "redirects to the sign-in page" do
                get :quick
                expect(response).to redirect_to login_path
            end
        end
    end
    describe "#demo" do
        it "resonds successfully" do
            get :demo
            expect(response).to be_successful
        end
    end
end
