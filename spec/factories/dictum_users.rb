FactoryBot.define do
  factory :dictum_user do
    sequence(:name) { |n| "user#{n}" }
    sequence(:email) { |n| "test#{n}@example.com" }
    password { "password" }
    password_confirmation { "password" }
    admin {false}

    trait :admin do
      admin {true}
    end
  end

  
end
# factory :music, class: Gogaku do
  #   subject {"English"}
  #   body {"in trip"}
  #   # after(:build) do |gogaku|
  #   #     gogaku.file = fixture_file_upload(Rails.root.join('spec', 'fixtures', 'files', 'Night_Sea.mp3'), 'audio/mpeg', :binary)
  #   # end
  #   file { fixture_file_upload(Rails.root.join('spec', 'fixtures', 'files', 'Night_Sea.mp3'), 'audio/mpeg') }
  #   answer {"none"}
  # end
# file {fixture_file_upload('test/fixtures/Night_Sea.mp3', 'audio/mpeg')}
# factory :kobe, class: DictumUser do
  #   name {"kobe"}
  #   email {"kobe@example.com"}
  #   password {"foobar"}
  #   password_confirmation {"foobar"}
  #   # gogaku
  # end

  # factory :lebron, class: DictumUser do
  #   name {"lebron"}
  #   email {"lebron@example.com"}
  #   password {"foobar"}
  #   password_confirmation {"foobar"}
  # end