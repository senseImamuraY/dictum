FactoryBot.define do
  factory :gogaku do
      subject {"English"}
      body {"in trip"}
      after(:build) do |gogaku|
          # gogaku.file = fixture_file_upload(Rails.root.join('spec', 'fixtures', 'files', 'Night_Sea.mp3'), 'audio/mp3', :binary)
          # g_file = Rails.root.join('spec', 'fixtures', 'files', 'Night_Sea.mp3')
          # gogaku.file.attach(io: File.open(g_file),filename: "Night_Sea.mp3",content_type: 'audio/mp3')
        image_file = File.open("./spec/fixtures/files/Night_Sea.mp3")
        gogaku.file.attach(io: image_file,filename: "Night_Sea.mp3",content_type: 'audio/mp3')
        # image_file.rewind
      end
      
      answer {"none"}
      association :dictum_user

      trait :file do
        file { DictumHelpers.upload_file('./spec/fixtures/files/Night_Sea.mp3', 'audio/mp3', true) }
      end
  end
end
# factory :music, class: Gogaku do
# trait(:recorded) do
      #   file { fixture_file_upload(Rails.root.join('spec', 'fixtures', 'files', 'Night_Sea.mp3'), 'audio/mpeg') }
      # end # end
