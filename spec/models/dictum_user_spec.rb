require 'rails_helper'

RSpec.describe DictumUser, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
	describe "test for DictumUser" do
		# let(:user) {DictumUser.new(params)}
		# let(:params) {{name: 				  name,
		# 			   email: 				  email,
		# 			   password: 			  password,
		# 			   password_confirmation: password_confirmation}}
    	
		
		context "is valid " do
				# let(:name) { "kobe" }
				# let(:email) { "kobe@example.com" }
				# let(:password) { "foobar" }
				# let(:password_confirmation) { "foobar" }
				# let(:user) { FactoryBot.build(:dictum_user) }
			it "create instance accurately" do
				
				# expect(user.name).to eq  "user{n}"
				# expect(user.email).to eq "kobe@example.com"
				# expect(user.password).to eq user.password_confirmation
				# expect(user).to be_valid
				expect(FactoryBot.build(:dictum_user)).to be_valid
			end
		end
    	context "when user input invalid parameter" do
			context "when name which user input is empty" do

				# let(:name) { " " }
				# let(:email) { "kobe@example.com" }
				# let(:password) { "foobar" }
				# let(:password_confirmation) { "foobar" }
				
				it " is invalid without name" do
					user = FactoryBot.build(:dictum_user, name: "")
					user.valid?
					expect(user.errors[:name]).to include("can't be blank")
					# expect(user).not_to be_valid
					# expect(FactoryBot.build(FactoryBot.build(:dictum_user, name: "")))
				end
			end
			context "when name which user input is too long" do
				# let(:name) { "a"*51}
				# let(:email) { "kobe@example.com" }
				# let(:password) { "foobar" }
				# let(:password_confirmation) { "foobar" }
				
				it " is invalid with too long name" do
					user = FactoryBot.build(:dictum_user, name: "a"*51)
					expect(user).not_to be_valid
				end
			end
			
			context "when email which user input is empty" do
				# let(:name) { "kobe" }
				# let(:email) { " " }
				# let(:password) { "foobar" }
				# let(:password_confirmation) { "foobar" }
				
				it "is invalid without email" do
					user = FactoryBot.build(:dictum_user, email: "")
					user.valid?
					expect(user.errors[:email]).to include("can't be blank")
					# expect(user).not_to be_valid
				end
			end
			context "when email which user input is invalid format" do
				# let(:name) { "kobe" }
				# let(:email) { "email.com "}
				# let(:password) { "foobar" }
				# let(:password_confirmation) { "foobar" }
				user = FactoryBot.build(:dictum_user, email: "email.com")
				it "is invalid with bad format" do
					expect(user).not_to be_valid
				end
			end
			context "when email which user input is too long" do
				# let(:name) { "kobe" }
				# let(:email) { "a"*244 + "@example.com"}
				# let(:password) { "foobar" }
				# let(:password_confirmation) { "foobar" }
				user = FactoryBot.build(:dictum_user, email: "a"*244+"@example.com")
				it "is invalid with too long email" do
					expect(user).not_to be_valid
				end
			end
			context "when password which user input is empty " do
				# let(:name) { "kobe" }
				# let(:email) { "kobe@example.com" }
				# let(:password) { " "*6 }
				# let(:password_confirmation) { " "*6 }
				user = FactoryBot.build(:dictum_user, password: " "*6, password_confirmation: " "*6)
				it "is invalid without password" do
					expect(user).not_to be_valid
				end
			end
			context "when password which user input is too short" do
				# let(:name) { "kobe" }
				# let(:email) { "kobe@example.com" }
				# let(:password) { "hoge" }
				# let(:password_confirmation) { "hoge" }
				user = FactoryBot.build(:dictum_user, password: "hoge", password_confirmation: "hoge")
				it "is invalid with too short password" do
					expect(user).not_to be_valid
				end
			end
			context "when email which user input isn`t unique" do
				# let!(:name) { "kobe" }
				# let!(:email) { "kobe@example.com" }
				# let!(:password) { "foobar" }
				# let!(:password_confirmation) { "foobar" }
				user = FactoryBot.build(:dictum_user)
				it "is invalid with a duplicate email" do
					user.save
					user2 = user.dup
					user2.email = user2.email.upcase
					user2.save
					user2.valid?
					expect(user2.errors[:email]).to include("has already been taken")
					# expect(user2).not_to be_valid
					user.destroy
				end
			end
			# context "when user is deleted" do
				
			# 	# let!(:kobe) { create(:music) }
			# 	let(:user) { FactoryBot.build(:dictum_user) }
			# 	# gogaku = FactoryBot.build(:gogaku, dictum_user: user)
			# 	# let(:gogaku) { FactoryBot.build(:gogaku) }
			# 	before do
			# 		# user.save
			# 		# user.gogakus.create(subject: "a",body: "a",file: Rack::Test::UploadedFile.new('spec/fixtures/files/Night_Sea.mp3', 'audio/mp3'),answer: "a",dictum_user: user)
			# 		gogaku = FactoryBot.create(:gogaku)
			# 	end
			# 	it " is deleted too" do
			# 		pending
			# 		# user.save
			# 		# gogaku.file = fixture_file_upload(Rails.root.join('spec', 'fixtures', 'files', 'Night_Sea.mp3'), 'audio/mpeg')
			# 		# pending
					
			# 		# gogaku.save
			# 		# user.gogakus.create(gogaku)
			# 		# expect(Gogaku.count).to eq 1
			# 		expect do 
			# 			user.destroy
			# 		end.to change(Gogaku, :count).by(-1)
					
			# 		# expect(Gogaku.count).to eq 0
			# 	end
			# end# user.gogakus.create!(subject: "English",body: "in trip",file:,answer:"none")

			

    	end
	end 	
end