require 'rails_helper'

RSpec.describe Gogaku, type: :model do
  # let(:dictum_user) { FactoryBot.build(:dictum_user) }
  
  describe "test for gogaku" do
    
    before do
      # dictum_user.save
      # let(:gogaku) { FactoryBot.build(:gogaku, dictum_user: dictum_user) }
      # dictum_user.gogakus.create(gogaku)
    end
    # dictum_user = DictumUser.new(name: "abc",email: "abc@example.com", password: "foobar", password_confirmation: "foobar")
    # let(:params) {FactoryBot.build(:gogaku)}
    # let(:params) {{subject: "a",body: "a",file: Rack::Test::UploadedFile.new('spec/fixtures/files/Night_Sea.mp3', 'audio/mp3'),answer: "a"}}
    # gogaku = dictum_user.gogakus.build({subject: "a",body: "a",file: Rack::Test::UploadedFile.new('spec/fixtures/files/Night_Sea.mp3', 'audio/mp3'),answer: "a"})
    # params.file.attach(io: File.open("/spec/fixtures/files/Night_Sea.mp3"),filename: "Night_Sea.mp3",content_type: 'audio/mp3')
    it " be valid" do
      # gogaku = dictum_user.gogakus.build(FactoryBot.build(:gogaku))
      # params.file.attach(io: File.open("/spec/fixtures/files/Night_Sea.mp3"),filename: "Night_Sea.mp3",content_type: 'audio/mp3')
      # gogaku = dictum_user.gogakus.build(params)
      # gogaku = FactoryBot.build(:gogaku, dictum_user: @dictum_user)
      gogaku = FactoryBot.create(:gogaku)
      expect(gogaku).to be_valid
      # expect do
      #   dictum_user.destroy
      # end.to change(Gogaku, :count).by(-1)
    end
    context "when user input invalid parameter" do
      it "is invalid without subject" do
        gogaku = FactoryBot.build(:gogaku, subject: "")
				gogaku.valid?
				expect(gogaku.errors[:subject]).to include("can't be blank")
      end
      it "is invalid without subject" do
        gogaku = FactoryBot.build(:gogaku, body: "")
				gogaku.valid?
				expect(gogaku.errors[:body]).to include("can't be blank")
      end
      it "is invalid without subject" do
        gogaku = FactoryBot.create(:gogaku, answer: "")
				expect(gogaku).to be_valid
      end

    end
  end
end
