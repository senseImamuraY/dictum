# RSpec.configure do |config|
#     config.before(:each, type: :system) do
#         driven_by :rack_test
#     end

#     config.before(:each, type: :system, js: true) do
#         Capybara.javascript_driver = :selenium_chrome
#         driven_by :selenium_chrome

#         # require 'selenium-webdriver'
#     #     caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {
#     #    "args" => %w(--headless --disable-gpu --no-sandbox)
#     #  })
#     #  driven_by :selenium, using: :chrome, options: { desired_capabilities: caps }
#     end
# end
RSpec.configure do |config|
    config.before(:each, type: :system) do
    #   driven_by :remote_chrome
        driven_by :rack_test
    #   Capybara.server_host = IPSocket.getaddress(Socket.gethostname)
    #   Capybara.server_port = 3000
    #   Capybara.app_host = "http://#{Capybara.server_host}:#{Capybara.server_port}"
    end
  
    config.before(:each, type: :system, js: true) do
      driven_by :remote_chrome
      Capybara.server_host = IPSocket.getaddress(Socket.gethostname)
      Capybara.server_port = 3001
      Capybara.app_host = "http://#{Capybara.server_host}:#{Capybara.server_port}"
    end
  end
  
  # Chrome
  Capybara.register_driver :remote_chrome do |app|
    url = 'http://chrome:4444/wd/hub'
    caps = ::Selenium::WebDriver::Remote::Capabilities.chrome(
      'goog:chromeOptions' => {
        'args' => [
          'no-sandbox',
        #   'headless',
          'disable-gpu',
          'window-size=1680,1050'
        ]
      }
    )
    Capybara::Selenium::Driver.new(app, browser: :remote, url: url, desired_capabilities: caps)
  end