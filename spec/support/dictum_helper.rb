module DictumHelpers
    def full_title(page_title = '')
        base_title = "Dictation_app DICTUM"
        if page_title.empty?
            base_title
        else
            page_title + " | " + base_title
        end
    end

    def gravatar_for(user)
        gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
        gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
        image_tag(gravatar_url, alt: user.name, class: "gravatar")
    end

    def log_in(user)
        session[:user_id] = user.id
    end

    # 現在ログイン中のユーザーを返す（いる場合）
    def current_user
        if session[:user_id]
            @current_user ||= DictumUser.find_by(id: session[:user_id])
        end
    end

    # ユーザーがログインしていればtrue、その他ならfalseを返す
    def logged_in?
        !current_user.nil?
    end

    # 現在のユーザーをログアウトする
    def log_out
        session.delete(:user_id)
        @current_user = nil
    end

    # 渡されたユーザーがカレントユーザーであればtrueを返す
    def current_user?(user)
        user && user == current_user
    end

    # 記憶したURL（もしくはデフォルト値）にリダイレクト
    def redirect_back_or(default)
        redirect_to(session[:forwarding_url] || default)
        session.delete(:forwarding_url)
    end

    # アクセスしようとしたURLを覚えておく
    def store_location
        session[:forwarding_url] = request.original_url if request.get?
    end

    def gogaku_params
        params.require(:gogaku).permit(:subject, :body, :file, :answer,)
    end

    def correct_user_g
        @gogaku = current_user.gogakus.find_by(id: params[:id])
        redirect_to root_url if @gogaku.nil?
    end

    def dictum_user_params
        params.require(:dictum_user).permit(:name, :email, :password,
                                    :password_confirmation)
      end

      # beforeアクション

    # ログイン済みユーザーかどうか確認
      # def logged_in_user
      #   unless logged_in?
      #     store_location
      #     flash[:danger] = "Please log in."
      #     redirect_to login_url
      #   end
      # end

      # 正しいユーザーかどうか確認
    def correct_user
        @dictum_user = DictumUser.find(params[:id])
        redirect_to(root_url) unless current_user?(@dictum_user)
    end

      # 管理者かどうか確認
    def admin_user
        redirect_to(root_url) unless current_user.admin?
    end

    def logged_in_user
        unless logged_in?
            store_location
            flash[:danger] = "Please log in."
            redirect_to login_url
        end
    end

    #ファイルのアタッチ変更
    def self.upload_file(src, content_type, binary = false)
        path = Rails.root.join(src)
        original_filename = ::File.basename(path)
    
        content = File.open(path).read
        tempfile = Tempfile.open(original_filename)
        tempfile.write content
        tempfile.rewind
    
        uploaded_file = Rack::Test::UploadedFile.new(tempfile, content_type, binary, original_filename: original_filename)
    
        ObjectSpace.define_finalizer(uploaded_file, uploaded_file.class.finalize(tempfile))
    
        uploaded_file
    end

end
   