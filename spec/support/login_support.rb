module TestSupport
    def sign_in_as
        visit root_path
        click_on "Log in"
        fill_in "session[email]",with:"ruby@example.com"
        fill_in "session[password]",with:"foobar"
        click_on "Log in!"
    end
    def sing_up_as
        visit root_path
        expect {
            click_link "Sign up now!"
            fill_in "dictum_user[name]", with:"ruby"
            fill_in "dictum_user[email]",with:"ruby@example.com"
            fill_in "dictum_user[password]",with:"foobar"
            fill_in "dictum_user[password_confirmation]",with:"foobar"
            click_on "Create my account"
        }.to change(DictumUser,:count).by(1)
    end
    def create_gogaku
        click_link "新しい音声を登録(最大5つまで)"
        expect {
            fill_in "gogaku[subject]", with: "英語"
            fill_in "gogaku[body]", with: "旅行先での英会話"
            attach_file "gogaku[file]","spec/fixtures/files/Night_Sea.mp3"
            fill_in "gogaku[answer]", with: "none"
            click_on "登録"
        }.to change(Gogaku,:count).by(1)
    end
end

RSpec.configure do |config|
    config.include TestSupport
end