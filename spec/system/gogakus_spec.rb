require 'rails_helper'

RSpec.describe "Gogakus", type: :system do
  # before do
  #   driven_by(:rack_test)
  # end

  # pending "add some scenarios (or delete) #{__FILE__}"
  scenario "user can use a sample" do
    # dictum_user = FactoryBot.create(:dictum_user)
    visit root_url
    click_link "Please try a sample!"
    click_button "スタート/ストップ"
    click_button "5秒進む"
    click_button "5秒戻る"
    find("option[value='0.5']").select_option
    find("option[value='0.75']").select_option
    find("option[value='1.25']").select_option
    find("option[value='1.5']").select_option
    find("option[value='1.75']").select_option
    find("option[value='1.0']").select_option
    click_button "区域ループ再生"
    click_button "スタート/ストップ"
    within "#header" do
       click_link "Help"
       click_link "About"
    end
  end
  scenario "gogaku life cycle" ,js: true do
    # gogaku = FactoryBot.create(:gogaku)
    visit root_path
    expect {
      click_link "Sign up now!"
      fill_in "dictum_user[name]", with:"ruby"
      fill_in "dictum_user[email]",with:"ruby@example.com"
      fill_in "dictum_user[password]",with:"foobar"
      fill_in "dictum_user[password_confirmation]",with:"foobar"
      click_on "Create my account"
  }.to change(DictumUser,:count).by(1)
      expect(page).to have_content "DICTUMへようこそ!"
      visit root_path
      click_link "クイックモード"
      within "#header" do
       click_link "Home"
      end 
      click_link "新しい音声を登録(最大5つまで)"
      expect {
        fill_in "gogaku[subject]", with: "英語"
        fill_in "gogaku[body]", with: "旅行先での英会話"
        attach_file "gogaku[file]","spec/fixtures/files/Night_Sea.mp3"
        fill_in "gogaku[answer]", with: "none"
        click_on "登録"
    }.to change(Gogaku,:count).by(1)
      expect(page).to have_content "登録完了!"
      click_link "Let's start studying!"
      within "#header" do
       click_link "Home"
      end 
      click_link "edit"
      click_on "更新"
      expect(page).to have_content "更新成功"
      click_link "delete"
      page.accept_confirm
      expect(page).to have_content "削除しました"
  end

  scenario "user login and logout" ,js: true do
      sing_up_as
      expect(page).to have_content "DICTUMへようこそ!"
      visit root_path
      click_on "Account"
      click_link "Log out"
      expect(page).to have_content "Please try a sample!"
      sign_in_as
  end

  scenario "user can't create gogaku more than 6" do
    sing_up_as
    create_gogaku
    create_gogaku
    create_gogaku
    create_gogaku
    create_gogaku
    expect(page).to have_content "新しい音声をこれ以上登録出来ません"
  end
end
