require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @dictum_user = dictum_users(:michael)
  end

  test "unsuccessful edit" do
    log_in_as(@dictum_user)
    get edit_dictum_user_path(@dictum_user)
    assert_template 'dictum_users/edit'
    patch dictum_user_path(@dictum_user), params: { dictum_user: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }

    assert_template 'dictum_users/edit'
  end

  test "successful edit" do
    get edit_dictum_user_path(@dictum_user)
    log_in_as(@dictum_user)
    assert_redirected_to edit_dictum_user_url(@dictum_user)
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch dictum_user_path(@dictum_user), params: { dictum_user: { name:  name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @dictum_user
    @dictum_user.reload
    assert_equal name,  @dictum_user.name
    assert_equal email, @dictum_user.email
  end
end
