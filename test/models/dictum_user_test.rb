require "test_helper"

class DictumUserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @dictum_user = DictumUser.new(name: "Example User", email: "user@example.com",
                    password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @dictum_user.valid?
  end

  test "name should be present" do
    @dictum_user.name = "     "
    assert_not @dictum_user.valid?
  end

  test "email shold be present" do
    @dictum_user.email = "     "
    assert_not @dictum_user.valid?
  end

  test "name should not be too long" do
    @dictum_user.name = "a" * 51
    assert_not @dictum_user.valid?
  end

  test "email should not be too long" do
    @dictum_user.email = "a" * 244 + "@example.com"
    assert_not @dictum_user.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @dictum_user.email = valid_address
      assert @dictum_user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @dictum_user.email = invalid_address
      assert_not @dictum_user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user = @dictum_user.dup
    duplicate_user.email = @dictum_user.email.upcase
    @dictum_user.save
    assert_not duplicate_user.valid?
  end

  test "password should be present (nonblank)" do
    @dictum_user.password = @dictum_user.password_confirmation = " " * 6
    assert_not @dictum_user.valid?
  end

  test "password should have a minimum length" do
    @dictum_user.password = @dictum_user.password_confirmation = "a" * 5
    assert_not @dictum_user.valid?
  end

  test "associated gogakus should be destoroyed" do
    @dictum_user.save
    @dictum_user.gogakus.create!(subject:"math",body:"linear algebra")
    assert_difference 'Gogaku.count', -1 do
      @dictum_user.destroy
    end
  end
end
