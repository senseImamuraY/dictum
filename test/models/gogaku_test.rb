require "test_helper"

class GogakuTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @dictum_user = dictum_users(:michael)
    @gogaku = @dictum_user.gogakus.build(subject:"programing", body:"ror", file:"#",answer:"none")
  end


  test "micael is valid?" do
    assert @dictum_user.valid?
  end

  test "should be valid" do
    assert @gogaku.valid?
  end

  test "user id should be present" do
    @gogaku.dictum_user_id = nil
    assert_not @gogaku.valid?
  end

  test "subject should be present" do
    @gogaku.subject = "      "
    assert_not @gogaku.valid?
  end

  test "body should be present" do
    @gogaku.body = "      "
    assert_not @gogaku.valid?
  end

  test "order should be most recent first" do
    assert_equal gogakus(:most_recent), Gogaku.first
  end
end
